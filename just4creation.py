import sqlite3
import random

deliminator = "^;^"


def problem_create():
    global deliminator
    con = sqlite3.connect("test.db")
    cur = con.cursor()
    cur.execute("CREATE TABLE qa(question, possibles, answer, category)")
    insert_statement = f"""INSERT INTO qa VALUES
    ('Their parents ________ engineers.', 'am{deliminator}has{deliminator}are{deliminator}is', 'c', 'gr'),
    ('He doesn’t have _______ idea.', 'a{deliminator}an{deliminator}some{deliminator}any', 'b', 'gr'),
    ('English First ______ a nice view.', 'is got{deliminator}have{deliminator}does have{deliminator}has', 'd', 'gr'),
    ('Notice  from English First institute, Visit English First institute in Abu Dhabi. The institute is open from Sunday to Friday, Welcome to English First institute. The Institute is closed', 'Monday{deliminator}Saturday{deliminator}Tuesday{deliminator}Friday', 'b', 'gr'),
    ('When do you play tennis? ____ Saturdays.', 'On{deliminator}In{deliminator}At{deliminator}By', 'a', 'gr'),
    ('____ two Hospitals in the city.', 'It is{deliminator}There is{deliminator}There are{deliminator}This is', 'c', 'gr'),
    ('He ____ video games  with me yesterday.', 'doesn’t played{deliminator}didn’t played{deliminator}not played{deliminator}didn’t play', 'd', 'gr'),
    ('Sorry, I ____ you at the moment.', 'can’t help{deliminator}don’t can help{deliminator}can’t helping{deliminator}can’t helps', 'a', 'gr'),
    ('We usually ____ the shopping in a supermarket.', 'make{deliminator}do{deliminator}have{deliminator}go', 'b', 'gr'),
    ('They hardly ____ visit us.', 'ever{deliminator}sometimes{deliminator}never{deliminator}usually', 'a', 'gr'),
    ('He ____ about basketball  which drives me mad!', 'forever talks{deliminator}is forever talking{deliminator}will forever be talking{deliminator}has forever been talking', 'b', 'gr'),
    ('Kim learns very fast ______ she is intelligent.', 'because{deliminator}or{deliminator}and{deliminator}but', 'a', 'gr'),
    ('He realized that he ____ his car keys in the office.', 'left{deliminator}has left{deliminator}had left{deliminator}is leaving', 'c', 'gr'),
    ('___ plans you might have for the weekend, you’ll have to change them.', 'Wherever{deliminator}Whovever{deliminator}Whatever{deliminator}However', 'c', 'gr'),
    ('You won’t pass the exam ____ you start studying immediately.', 'as long as{deliminator}provided{deliminator}unless{deliminator}if', 'c', 'gr'),
    ('What time is it?', 'It’s six and half{deliminator}It’s half six{deliminator}It’s half past six{deliminator}It’s a half six', 'c', 'gr'),
    ('The police stopped us and said we ____ to enter the building.', 'can’t{deliminator}couldn’t{deliminator}didn’t allow{deliminator}weren’t allowed', 'd', 'gr'),
    ('The view wasn’t just nice! It was ____ astonishing!', 'sort of{deliminator}absolutely{deliminator}too{deliminator}fairly', 'b', 'gr'),
    ('____ the weather was horrible, we decided to go out for a short walk.', 'Even though{deliminator}However{deliminator}In spite of{deliminator}Despite', 'a', 'gr'),
    ('The square was ____ crowded we couldn’t pass.', 'so{deliminator}such{deliminator}very{deliminator}as', 'a', 'gr'),
    ('Two climbers are reported to ____ during the storm last night.', 'die{deliminator}have died{deliminator}had died{deliminator}died', 'b', 'gr'),
    ('John is ______________ .', 'my sister’s friend{deliminator}friend my sister{deliminator}friend from my sister{deliminator}my sister friend’s', 'a', 'gr'),
    ('The living room is ___________________ than the bedroom.', 'more big{deliminator}more bigger{deliminator}biggest{deliminator}bigger', 'd', 'gr'),
    ('________________ seen fireworks before?', 'Did you ever{deliminator}Are you ever{deliminator}Have you ever{deliminator}Do you ever', 'c', 'gr'),
    ('We’ve been friends ____________________ many years.', 'since{deliminator}from{deliminator}during{deliminator}for', 'd', 'gr'),
    ('These are the photos ________________ I took on holiday.', 'which{deliminator}who{deliminator}what{deliminator}where', 'a', 'gr'),
    ('I would like ______ butter on my toast, please.', 'any{deliminator}a{deliminator}some{deliminator}a bit', 'c', 'gr'),
    ('I wasn’t interested in the performance very much. ______ .', 'I didn’t, too.{deliminator}Neither was I.{deliminator}Nor I did.{deliminator}So I wasn’t.', 'b', 'gr'),
    ('Take a warm coat, __________ you might get very cold outside.', 'otherwise{deliminator}in case{deliminator}so that{deliminator}in order to', 'a', 'gr'),
    ('What I like more than anything else __________ at weekends.', 'playing golf{deliminator}to play golf{deliminator}is playing golf{deliminator}is play golf', 'c', 'gr'),
    ('She _________ for her cat for two days when she finally found it in the garage.', 'looked{deliminator}had been looked{deliminator}had been looking{deliminator}were looking', 'c', 'gr'),
    ('He doesn’t have ______ money.', 'much{deliminator}a lot{deliminator}many{deliminator}plenty', 'a', 'gr'),
    ('If he ______  about it, I’m sure he’d help.', 'had know{deliminator}knew{deliminator}has known{deliminator}knows', 'b', 'gr'),
    ('It’s all right, we ______ hurry. We have plenty of time.', 'must{deliminator}shouldn’t{deliminator}can’t{deliminator}needn’t', 'd', 'gr'),
    ('The financial director ______ for almost an hour.', 'kept us to wait{deliminator}kept us waiting{deliminator}made us wait{deliminator}made us waiting', 'b', 'gr'),
    ('Latin ____ compulsory in Irish schools.', 'used to be{deliminator}would be{deliminator}has{deliminator}has been', 'a', 'gr'),
    ('I ____ to Peru on holiday next month.', 'am flying {deliminator}flying{deliminator}am go flying{deliminator}will flying', 'a', 'gr'),
    ('This book is ______ .', 'my{deliminator}mine{deliminator}your{deliminator}me', 'b', 'gr'),
    ('We wouldn’t have missed the bus if you ____ to chat with Mary!', 'didn’t stop{deliminator}hadn’t stopped{deliminator}don’t stop{deliminator}wouldn’t have stopped', 'b', 'gr'),
    ('The party was so boring I wish I ____  at all.', 'hadn’t gone{deliminator}wouldn’t go{deliminator}haven’t gone{deliminator}didn’t go', 'a', 'gr'),
    ('She __________ a lot of her free time reading.', 'does{deliminator}spends{deliminator}has{deliminator}makes', 'b', 'gr'),
    ('Hello, this is Steve. Could I ____________ to Jane, please?', 'say{deliminator}tell{deliminator}call{deliminator}speak', 'd', 'gr'),
    ('I only paid  AED 20 for this jacket! It was a real ____ .', 'buy{deliminator}price{deliminator}bargain{deliminator}sale', 'c', 'vo'),
    ('The firework ____ was really amazing.', 'exhibition{deliminator}display{deliminator}collection{deliminator}vision', 'b', 'vo'),
    ('Paul is always lost in his thoughts. He is _____ .', 'self-centered{deliminator}self-confident{deliminator}self-conscious{deliminator}self-absorbed', 'd', 'vo'),
    ('She doesn’t ____ of my decision.', 'agree{deliminator}approve{deliminator}accept{deliminator}support', 'b', 'vo'),
    ('During his stay in Indonesia he went ____ with malaria.', 'up{deliminator}off{deliminator}down{deliminator}over', 'c', 'vo'),
    ('I can’t move the sofa. Could you ____ me a hand with it, please?', 'give{deliminator}get{deliminator}take{deliminator}borrow', 'a', 'vo'),
    ('You may not like the cold weather here, but you’ll have to ________________ , I’m afraid.', 'tell it off{deliminator}sort itself out{deliminator}put up with it{deliminator}put it off', 'c', 'vo'),
    ('She ______ at me and then turned away.', 'viewed{deliminator}regarded{deliminator}watched{deliminator}glanced', 'd', 'vo'),
    ('______ tired Melissa is when she gets home from work, she always makes time to say goodnight to the children.', 'Whatever{deliminator}No matter how{deliminator}However much{deliminator}Although', 'b', 'vo'),
    ('It was only ten days ago ______ she started her new job.', 'then{deliminator}since{deliminator}after{deliminator}that', 'd', 'vo'),
    ('Have you got time to discuss your work now, or are you ______ to leave?', 'thinking{deliminator}round{deliminator}planned{deliminator}about', 'd', 'vo'),
    ('Once the plane is in the air, you can ______ your seat belts if you wish.', 'undress{deliminator}unfasten{deliminator}unlock{deliminator}untie', 'b', 'vo'),
    ('I’d rather  ______ to her why we can’t go.', 'would explain{deliminator}explain{deliminator}to explain{deliminator}will explain', 'b', 'vo'),
    ('When I realized I had dropped my gloves, I decided to ______ my steps.', 'retrace{deliminator}regress{deliminator}resume{deliminator}return', 'a', 'vo'),
    ('A lot of ____ came to Ireland in the 1990s.', 'immigrants{deliminator}emigrants{deliminator}invaders{deliminator}colonies', 'a', 'vo'),
    ('She’s very successful. Her ___ has risen a lot in the past few years.', 'work{deliminator}salary{deliminator}job{deliminator}earnings', 'd', 'vo'),
    ('He ___ his exam because he didn’t study.', 'failed{deliminator}disapprove{deliminator}missed{deliminator}fell behind', 'a', 'vo'),
    ('He ____ off his holiday until after the winter.', 'took{deliminator}put{deliminator}called{deliminator}logged', 'b', 'vo')"""

    cur.execute(insert_statement)
    con.commit()

def sel_probs():
    con = sqlite3.connect("test.db")
    cur = con.cursor()

    res = cur.execute("SELECT * FROM qa")
    res_arr = []
    for i in res:
        res_arr.append(i)

    random.shuffle(res_arr)
    print(res_arr)
