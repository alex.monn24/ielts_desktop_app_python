import eel
from eel import browsers
import my_chrome as m_chm
import sqlite3
import random
from threading import Timer

run = True

# name of folder where the html, css, js, image files are located
eel.init('templates')

total_min = 0.5
total_sec = total_min * 60
current_remaining = total_sec

name = ""
mobile = ""
email = ""

questsAndAnswers = []
testState = []

totalNumberOfQuests = 0
fullScore = 60

abnormalActionCount = 0

vocabulary_total = 0
grammar_total = 0
vocabulary_correct = 0
grammar_correct = 0
answered = 0


def test():
    global run, current_remaining, tl
    current_remaining = current_remaining - 1
    eel.updateTimeIndication(current_remaining)
    if current_remaining <= 0:
        eel.goResult()
    if run:
        Timer(1, test).start()


@eel.expose
def stopTimer():
    global tl, current_remaining, run
    current_remaining = total_sec
    # tl.stop()
    run = False


@eel.expose
def demo(x):
    return x


@eel.expose
def totalTimeGetter():
    global total_sec
    return total_sec


@eel.expose
def totalQuestNumGetter():
    global totalNumberOfQuests
    return totalNumberOfQuests


@eel.expose
def getAnswerState(current_quest_num):
    global testState
    return testState[current_quest_num]


@eel.expose
def saveInfoAndContinue(pname, pmobile, pemail):
    global name, mobile, email
    name = pname
    mobile = pmobile
    email = pemail
    return True


@eel.expose
def prepareQuest():
    global questsAndAnswers, testState, totalNumberOfQuests, vocabulary_total, grammar_total
    con = sqlite3.connect("test.db")
    cur = con.cursor()

    grammar_total = 0
    vocabulary_total = 0
    res = cur.execute("SELECT * FROM qa")
    res_arr = []
    for i in res:
        res_arr.append(i)
        if i[3] == "gr":
            grammar_total += 1
        elif i[3] == "vo":
            vocabulary_total += 1

    random.shuffle(res_arr)
    questsAndAnswers = res_arr
    # initialization of testState
    testState = []
    for i in questsAndAnswers:
        testState.append(None)

    totalNumberOfQuests = len(questsAndAnswers)


@eel.expose
def prepareAnswer():
    global questsAndAnswers, testState, totalNumberOfQuests


@eel.expose
def displayOneQuest(i):
    eel.fetchOnPage(i, questsAndAnswers[i])


@eel.expose
def startTimer():
    global run
    run = True

    # tl.start()
    test()


@eel.expose
def setAnswerState(current_quest_num, choice):
    testState[current_quest_num] = choice
    # print(testState)


@eel.expose
def calculateScore():
    global totalNumberOfQuests, name, mobile, email, grammar_correct, vocabulary_correct, answered, grammar_total, vocabulary_total
    ind = 0
    correctAnswerNum = 0
    grammar_correct = 0
    vocabulary_correct = 0
    answered = 0

    for i in questsAndAnswers:
        if i[2] == testState[ind]:
            correctAnswerNum += 1
            if i[3] == "gr":
                grammar_correct += 1
            elif i[3] == "vo":
                vocabulary_correct += 1

        if testState[ind] is not None:
            answered += 1

        ind += 1
    score = [correctAnswerNum, fullScore, name, mobile, email, grammar_correct, vocabulary_correct, answered, grammar_total, vocabulary_total]
    return score


@eel.expose
def abnormal():
    global abnormalActionCount
    abnormalActionCount += 1
    if abnormalActionCount > 2:
        abnormalActionCount = 0
        eel.gotoHalted()


browsers._browser_modules['my_chrome'] = m_chm
eel.start('index.html', mode='my_chrome', host='localhost', port=61685, block=True,
          cmdline_args=['--start-maximized', '--disable-add-to-shelf', '--disable-notifications',
                        '--ash-hide-notifications-for-factory'])
