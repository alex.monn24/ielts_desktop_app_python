var totalSec
var totalQuestNum
var current_quest_num
var temp_current_answer

function timedRedirect(){
    var timeToRedirect = 7
    var redirecter = setTimeout(()=>{
        window.location.replace("./subscribe.html")
    }, 1000 * timeToRedirect)
}

function saveInfoAndContinue(){
    var name = document.getElementById("name").value
    var mobile = document.getElementById("mobile").value
    var email = document.getElementById("email").value

    eel.saveInfoAndContinue(name, mobile, email)(continueIfSuccess)
}

function continueIfSuccess(res){
    if (res == true){
        window.location.replace("./before_test.html")
    }
}

function beginTest(){
    window.location.replace("./test.html")
}


function confirm(){
    var confirmation = document.getElementById("flexCheckDefault").checked
    document.getElementById("begin_test").disabled = !confirmation
}

function prepareAnswer(){
    eel.displayOneQuest(0)
    eel.totalQuestNumGetter()(getTotalQuestNum)
}

function prepareQuest(){
    eel.prepareQuest()
    startTestWorkers()
}

function startTestWorkers(){
    eel.displayOneQuest(0)
    eel.totalTimeGetter()(getTotalSec)
    eel.totalQuestNumGetter()(getTotalQuestNum)
    eel.startTimer()
}

function getTotalSec(s){
    totalSec = s
}

function getTotalQuestNum(s){
    totalQuestNum = s
}


eel.expose(fetchOnPage);
function fetchOnPage(num, qa_array) {
    current_quest_num = num
    document.getElementById("quest_num").innerHTML = parseInt(num) + 1
    document.getElementById("quest").innerHTML = qa_array[0]
    var answer_array = qa_array[1].split("^;^")
    document.getElementById("ans_a").innerHTML = answer_array[0]
    document.getElementById("ans_b").innerHTML = answer_array[1]
    document.getElementById("ans_c").innerHTML = answer_array[2]
    document.getElementById("ans_d").innerHTML = answer_array[3]

    eel.getAnswerState(current_quest_num)(answerState)
}

function answerState(s){
    var btn_primary = document.getElementsByClassName("btn-primary")
    var id
    if(btn_primary[0] != undefined){
        btn_primary[0].classList.add('btn-light')
        btn_primary[0].classList.remove('btn-primary')
    }

    temp_current_answer = s
    if (temp_current_answer != null){
        document.getElementById("ans_" + temp_current_answer).parentElement.classList.remove("btn-light")
        document.getElementById("ans_" + temp_current_answer).parentElement.classList.add("btn-primary")
    }
}

eel.expose(updateTimeIndication);
function updateTimeIndication(current_remaining){
    var min = Math.floor(current_remaining / 60)
    if (min < 10){
        min = "0" + min.toString()
    }
    var sec = current_remaining % 60
    if (sec < 10){
        sec = "0" + sec.toString()
    }
    var formattedRemainingTime = min.toString() + " : " + sec.toString()
    document.getElementById("remaining_indicator").innerHTML = formattedRemainingTime
    document.getElementById("time_progress_bar").style.width = (100 - (parseInt(current_remaining) / parseInt(totalSec) * 100)) + "%"
    if(100 - (parseInt(current_remaining) / parseInt(totalSec) * 100) >= 80){
        if(!document.getElementById("time_progress_bar").parentElement.classList.contains("progress_bar_pulsate")){
            document.getElementById("time_progress_bar").parentElement.classList.add("progress_bar_pulsate")
        }
    }
}

function setAnswerState(e){
    var btn_primary = document.getElementsByClassName("btn-primary")
    var id
    if(btn_primary[0] != undefined){
        btn_primary[0].classList.add('btn-light')
        btn_primary[0].classList.remove('btn-primary')
    }

    if (e.target.tagName == "BUTTON"){
        e.target.classList.remove("btn-light")
        e.target.classList.add("btn-primary")
        id = e.target.firstElementChild.id
    }else{
        e.target.parentElement.classList.remove("btn-light")
        e.target.parentElement.classList.add("btn-primary")
        id = e.target.id
    }

    var choice = id.replace("ans_","")

    eel.setAnswerState(current_quest_num, choice)
}


function fetchNextQuest(){
    var btn_primary = document.getElementsByClassName("btn-primary")
    if(btn_primary[0] == undefined){
        return
    }
    var next_num = current_quest_num + 1
    var path = window.location.pathname;
    var page = path.split("/").pop();
    if (next_num >= totalQuestNum){
        if(page == "test.html"){
            document.getElementById("end_modal_launcher").dispatchEvent(new Event("click"))
        }else if(page == "check_answer.html"){
            return
        }
        return
    }

    eel.displayOneQuest(next_num)
    var quest_progress_bar = document.getElementById("quest_progress_bar")
    var quest_progression = Math.ceil((current_quest_num + 2) / totalQuestNum * 100)
    quest_progress_bar.style.width = quest_progression + "%"
    quest_progress_bar.innerHTML = (current_quest_num + 2) + " / " + totalQuestNum
}

function fetchPrevQuest(){
    var prev_num = current_quest_num - 1
    if (prev_num < 0){
        return
    }

    eel.displayOneQuest(prev_num)
    var quest_progress_bar = document.getElementById("quest_progress_bar")
    var quest_progression = Math.ceil((current_quest_num ) / totalQuestNum * 100)
    quest_progress_bar.style.width = quest_progression + "%"
    quest_progress_bar.innerHTML = (current_quest_num ) + " / " + totalQuestNum
}



////////////////////////////////////////////////////////////////////////////////////
eel.expose(goResult)
function goResult(){
    eel.stopTimer()
    window.location.replace("./result.html")
}

function displayScore(){
    eel.calculateScore()(getScore)
}

function getScore(s){
    document.getElementById("score").innerHTML = s[0]
    document.getElementById("full_score").innerHTML = s[1]
    document.getElementById("user_name").innerHTML = s[2]
    document.getElementById("user_name_1").innerHTML = s[2]
    document.getElementById("user_mobile").innerHTML = s[3]
    document.getElementById("user_mail").innerHTML = s[4]

    var ef_level, ielts_score, vocabulary_percentage, grammar_percentage, average_percentage, answered, correct, wrong
    // [correctAnswerNum, fullScore, name, mobile, email, grammar_correct, vocabulary_correct, answered, grammar_total, vocabulary_total]

    if(parseInt(s[0]) == 0 ){
        ef_level = 0
        ielts_score = 0
    }else if(parseInt(s[0]) >= 1 && parseInt(s[0]) <= 4){
        ef_level = "Foundation"
        ielts_score = "2"
    }else if(parseInt(s[0]) >= 5 && parseInt(s[0]) <= 9){
        ef_level = "Level 1"
        ielts_score = "2"
    }else if(parseInt(s[0]) >= 10 && parseInt(s[0]) <= 14){
        ef_level = "Level 2"
        ielts_score = "2"
    }else if(parseInt(s[0]) >= 15 && parseInt(s[0]) <= 19){
        ef_level = "Level 3"
        ielts_score = "3"
    }else if(parseInt(s[0]) >= 20 && parseInt(s[0]) <= 24){
        ef_level = "Level 4"
        ielts_score = "4 - 4.5"
    }else if(parseInt(s[0]) >= 25 && parseInt(s[0]) <= 29){
        ef_level = "Level 5"
        ielts_score = "4 - 4.5"
    }else if(parseInt(s[0]) >= 30 && parseInt(s[0]) <= 34){
        ef_level = "Level 6"
        ielts_score = "5 - 5.5"
    }else if(parseInt(s[0]) >= 35 && parseInt(s[0]) <= 39){
        ef_level = "Level 7"
        ielts_score = "5.5 - 6.5"
    }else if(parseInt(s[0]) >= 40 && parseInt(s[0]) <= 44){
        ef_level = "Level 8"
        ielts_score = "6.5 - 7"
    }else if(parseInt(s[0]) >= 45 && parseInt(s[0]) <= 49){
        ef_level = "Level 9"
        ielts_score = "7 - 8"
    }else if(parseInt(s[0]) >= 50 && parseInt(s[0]) <= 54){
        ef_level = "Level 10"
        ielts_score = "7 - 8"
    }else if(parseInt(s[0]) >= 55 && parseInt(s[0]) <= 60){
        ef_level = "Level 11"
        ielts_score = "7 - 8"
    }

    vocabulary_percentage = parseInt(s[6]) / parseInt(s[9]) * 100
    vocabulary_percentage = vocabulary_percentage.toFixed(2)

    grammar_percentage = parseInt(s[5]) / parseInt(s[8]) * 100
    grammar_percentage = grammar_percentage.toFixed(2)

    average_percentage = (Number(vocabulary_percentage) + Number(grammar_percentage)) / 2
    average_percentage = average_percentage.toFixed(2)

    answered = s[7]
    correct = s[0]
    incorrect = answered - correct

    document.getElementById("ef_level").innerHTML = ef_level
    document.getElementById("ielts_score").innerHTML = ielts_score
    document.getElementById("vocabulary_percentage").innerHTML = vocabulary_percentage + " %"
    document.getElementById("grammar_percentage").innerHTML = grammar_percentage + " %"
    document.getElementById("average_percentage").innerHTML = average_percentage + " %"
    document.getElementById("answered").innerHTML = answered + " / " + s[1]
    document.getElementById("correct").innerHTML = correct + " / " + answered
    document.getElementById("incorrect").innerHTML = incorrect + " / " + answered

}
///////////////////////////////////////////////////////////////////////////////////
function checkAnswer(){
    window.location.replace("./check_answer.html")
}

function printCertificate(){
    window.print()
}
///////////////////////////////////////////////////////////////////////////////////
function showAlert(e) {
  document.getElementById("liveToast").classList.add("showing")
  eel.abnormal()
}

function hideAlert(){
  document.getElementById("liveToast").classList.remove("showing")
}

eel.expose(gotoHalted)
function gotoHalted(){
    eel.stopTimer()
    window.location.replace("./halted.html")
}




function compute() {
    var data = document.getElementById("data").value
    eel.demo(data)(setValue) // call the demo function which we have created in the main.py file
}

function setValue(res) {
    document.getElementById("abc").innerHTML = res
}